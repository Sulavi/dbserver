#include <string>
#include <iostream>
#include <map>
#include <set>
#include <zmq.hpp>
#include "../dbcommon/deckbuilder.pb.h"
#include "../dbcommon/zhelpers.hpp"

//#include <google/protobuf/util/json_util.h>
//#include <openssl/md5.h>

/*
 * Hosts server side decks
 * Decks are indexed by name
 * When a client tries to connect to a deck that doesn't exist, one is created
 * The true version of the deck is stored on the server.
 * When a client sends an update, it contains a hash of the current deck.
 * If the hash does not match the state of the server deck, the update is rejected and
 * the current state is sent to that client.
 * When the state is sucessfully updated, the new state is sent to all clients.
 * When there are no clients connected, the deck is deleted.
*/

using namespace db;


template<typename T, typename U>
bool contains(std::map<T, U> m, T t){
    for(const auto& [key, value] : m){
        if(key == t){
            return true;
        }
    }

    return false;
}

//std::string generateHash(const Deck * deck){
//    unsigned char result[MD5_DIGEST_LENGTH+1] = {0};
//    std::string deckStr = deck->SerializeAsString();
//    MD5((const unsigned char*)deckStr.c_str(), deckStr.size(), result);
//    return std::string((char*)(result));
//}

struct ServerDeck{
    Deck deck;
    std::set<std::string> subscribers;
};

std::map<std::string, ServerDeck> decks;

int main()
{
    std::cout << "Deck Builder Server Started.\n";

    zmq::context_t ctx;
    zmq::socket_t router(ctx, zmq::socket_type::router);

    int port = 5558;
    std::cout << "Listening on port " << port;
    router.bind("tcp://*:" + std::to_string(port));

    DBMessage dbm;

    for(;;){
        // Get message
        std::string id = s_recv(router);// Client id
        s_recv(router); // Empty frame
        dbm.ParseFromString(s_recv(router));

        // Deck Subscription
        if(dbm.has_subscribe()){
            // Add subscriber to table
            // Create a new deck if it doesn't exist
            Subscribe subscribe = dbm.subscribe();
            decks[subscribe.index()].subscribers.insert(id);

            // Respond with deck
            Deck subscribeAck;
            subscribeAck.CopyFrom(decks[subscribe.index()].deck);
            s_sendmore(router, id);
            s_sendmore(router, std::string(""));
            s_send(router, subscribeAck.SerializeAsString());

        // Deck Unsubscription
        } else if(dbm.has_unsubscribe()){
            Unsubscribe unsubscribe = dbm.unsubscribe();
            if(contains(decks, unsubscribe.index())){
                decks[unsubscribe.index()].subscribers.erase(id);
                // Delete deck if there are no subscribers
                if(decks[unsubscribe.index()].subscribers.size() <= 0 ){
                    decks.erase(unsubscribe.index());
                }
            }

        // Deck Update
        } else if(dbm.has_update()){
            Update update = dbm.update();
            Deck* deck = &decks[update.index()].deck;
            if(update.count() == deck->count()){
                // Update deck
                switch (update.type()) {
                case ADD_CARD:
                    deck->add_cards()->CopyFrom(update.card(0));
                    deck->set_count(deck->count()+1);
                    break;
                case REMOVE_CARD:
                    if(update.cardindex() < deck->cards_size()){
                        for(int i=update.cardindex(); i < deck->cards_size()-1; i++){
                            deck->mutable_cards(i)->CopyFrom(deck->cards(i+1));
                        }
                        deck->mutable_cards()->RemoveLast();
                        deck->set_count(deck->count()+1);
                    }
                    break;
                case REMAKE:
                    deck->clear_cards();
                    for(int i=0; i<update.card_size(); i++){
                        deck->add_cards()->CopyFrom(update.card(i));
                    }
                    deck->set_count(deck->count()+1);
                    break;
                case SET_PARAMETERS:
                    deck->set_ordered(update.ordered());
                    deck->set_hidden(update.hidden());
                    deck->set_count(deck->count()+1);
                    break;
                case NONE:
                    std::cout << "Received update of type none, dropping.\n";
                }


                // Send new state to all clients
                for(std::string client : decks[update.index()].subscribers){
                    s_sendmore(router, client);
                    s_sendmore(router, std::string(""));
                    s_send(router, deck->SerializeAsString());
                }
            } else {
                // If update is invalid, send current state
                Deck deckState;
                deckState.CopyFrom(*deck);
                s_sendmore(router, id);
                s_sendmore(router, std::string(""));
                s_send(router, deckState.SerializeAsString());
            }
        }
    }

    return 0;
}
