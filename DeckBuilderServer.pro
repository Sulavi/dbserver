TEMPLATE = app
CONFIG += console c++20
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lprotobuf -lzmq -pthread

SOURCES += \
        ../dbcommon/deckbuilder.pb.cc \
        main.cpp

HEADERS += \
    ../dbcommon/deckbuilder.pb.h \
    ../dbcommon/zhelpers.hpp

DISTFILES += \
    ../dbcommon/deckbuilder.proto
